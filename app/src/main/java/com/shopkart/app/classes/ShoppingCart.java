package com.shopkart.app.classes;

import android.content.Context;

import com.shopkart.app.Callback;
import com.shopkart.app.SKError;
import com.shopkart.app.SKResponse;
import com.shopkart.app.database.ShopCartDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by salil on 2/3/16.
 */
public class ShoppingCart {

    private List<Product> productList = null;
    private Amount totalAmount;
    private Context mContext;
    private ShopCartDatabaseHelper mShopCartDatabaseHelper;

    public static ShoppingCart cart = null;

    private ShoppingCart(Context context) {
        this.mContext = context;
        productList = new ArrayList<>();
        totalAmount = new Amount(0);

        mShopCartDatabaseHelper = new ShopCartDatabaseHelper(mContext);
//        getProductList();
    }

    public static ShoppingCart getCart(Context context) {
        if (cart == null) {
            synchronized (ShoppingCart.class) {
                if (cart == null) {
                    cart = new ShoppingCart(context);
                }
            }
        }

        return cart;
    }

    /**
     * Add product to the cart.
     *
     * @param product
     */
    public synchronized void addProductToCart(Product product, Callback<SKResponse> callback) {
        long rowId = -1;
        // Add product in the database.
        if (mShopCartDatabaseHelper != null) {
            rowId = mShopCartDatabaseHelper.addProductToCart(product);
        }

        if (rowId != -1) {
            Utils.sendResponse(callback, new SKResponse(Constants.MESSAGE_PRODUCT_ADDED_TO_CART));
        } else {
            Utils.sendError(callback, new SKError(Constants.ERROR_MESSAGE_PRODUCT_ADDED_TO_CART));
        }
    }

    /**
     * Remove product from the cart.
     *
     * @param product
     */
    public synchronized void removeProductFromCart(Product product, Callback<SKResponse> callback) {
        if (productList != null) {
            if (mShopCartDatabaseHelper != null) {
                boolean removed = mShopCartDatabaseHelper.removeProductFromCart(product);

                if (removed) {
                    Utils.sendResponse(callback, new SKResponse(Constants.MESSAGE_PRODUCT_REMOVED_FROM_CART));
                } else {
                    Utils.sendError(callback, new SKError(Constants.ERROR_MESSAGE_PRODUCT_REMOVED_FROM_CART));
                }

            }
        }
    }

    /**
     * Fetch products from db.
     * It will return the product list for cart. If the product list is empty then it will return an error.
     */
    public synchronized void getProductList(Callback<List<Product>> callback) {
        productList = mShopCartDatabaseHelper.getAllProductFromCart();
        if (productList != null && !productList.isEmpty()) {
            Utils.sendResponse(callback, productList);
        } else {
            Utils.sendError(callback, new SKError(Constants.ERROR_MESSAGE_EMPTY_CART));
        }
    }

    public Amount getTotalAmount() {
        return calculatedPrice();
    }

    private Amount calculatedPrice() {
        double totalPrice = 0.0d;
        if (productList != null) {
            for (Product product : productList) {
                if (product != null) {
                    Amount price = product.getPrice();
                    totalPrice += price.getValue() * product.getQuantity();
                }
            }
        }

        return new Amount(totalPrice);
    }

    public void destroy() {
        mContext = null;
        totalAmount = null;
        productList = null;
        if (mShopCartDatabaseHelper != null) {
            mShopCartDatabaseHelper.close();
        }
    }
}
