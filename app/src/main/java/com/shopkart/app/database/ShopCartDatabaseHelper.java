package com.shopkart.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.shopkart.app.classes.Amount;
import com.shopkart.app.classes.Product;
import com.shopkart.app.database.CartDataContract.CartEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by salil on 4/3/16.
 */
public class ShopCartDatabaseHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private static final String INT_TYPE = " REAL";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + CartEntry.TABLE_NAME + " (" +
                    CartEntry._ID + " INTEGER" + COMMA_SEP +
                    CartEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    CartEntry.COLUMN_NAME_PRICE + REAL_TYPE + COMMA_SEP +
                    CartEntry.COLUMN_NAME_IMAGE_URL + TEXT_TYPE + " PRIMARY KEY " + COMMA_SEP +
                    CartEntry.COLUMN_NAME_CATEGORY + TEXT_TYPE + COMMA_SEP +
                    CartEntry.COLUMN_NAME_QUANTITY + INT_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + CartEntry.TABLE_NAME;

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "ShopKart.db";

    public ShopCartDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**
     * Add product to the cart.
     *
     * @param product
     * @return rowId for the newly added product
     */
    public long addProductToCart(Product product) {
        if (product != null) {
            SQLiteDatabase db = getWritableDatabase();

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(CartEntry.COLUMN_NAME_TITLE, product.getName());
            values.put(CartEntry.COLUMN_NAME_PRICE, product.getPrice().getValue());
            values.put(CartEntry.COLUMN_NAME_IMAGE_URL, product.getImageUrl());
            values.put(CartEntry.COLUMN_NAME_CATEGORY, product.getCategory());
            values.put(CartEntry.COLUMN_NAME_QUANTITY, 1);

            // Insert the new row, returning the primary key value of the new row
            long rowId = db.insert(
                    CartEntry.TABLE_NAME,
                    null,
                    values);

            db.close();

            return rowId;
        }

        return -1;
    }

    public List<Product> getAllProductFromCart() {
        List<Product> productList = null;
        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {
                CartEntry.COLUMN_NAME_TITLE,
                CartEntry.COLUMN_NAME_PRICE,
                CartEntry.COLUMN_NAME_IMAGE_URL,
                CartEntry.COLUMN_NAME_CATEGORY,
                CartEntry.COLUMN_NAME_QUANTITY
        };

        Cursor c = db.query(
                CartEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        while (c.moveToNext()) {
            String title = c.getString(c.getColumnIndex(CartEntry.COLUMN_NAME_TITLE));
            double price = c.getDouble(c.getColumnIndex(CartEntry.COLUMN_NAME_PRICE));
            String imageUrl = c.getString(c.getColumnIndex(CartEntry.COLUMN_NAME_IMAGE_URL));
            String category = c.getString(c.getColumnIndex(CartEntry.COLUMN_NAME_CATEGORY));
            int quantity = c.getInt(c.getColumnIndex(CartEntry.COLUMN_NAME_QUANTITY));

            if (productList == null) {
                productList = new ArrayList<>();
            }

            Product product = new Product(title, new Amount(price), imageUrl, category, quantity);
            productList.add(product);
        }

        db.close();

        return productList;
    }

    /**
     * Remove product from the cart.
     *
     * @param product
     * @return true if the product is removed else false.
     */
    public boolean removeProductFromCart(Product product) {
        boolean removed = false;
        if (product != null) {
            SQLiteDatabase db = getWritableDatabase();

            String whereClause = CartEntry.COLUMN_NAME_IMAGE_URL + " = ?";
            String[] whereArgs = {product.getImageUrl()};

            int rowsAffected = db.delete(CartEntry.TABLE_NAME, whereClause, whereArgs);

            if (rowsAffected > 0) {
                removed = true;
            }

            db.close();
        }

        return removed;
    }
}