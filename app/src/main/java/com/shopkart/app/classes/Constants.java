package com.shopkart.app.classes;

/**
 * Created by salil on 2/3/16.
 */
public interface Constants {
    String URL_PRODUCT_LIST = "https://salty-plateau-1529.herokuapp.com/product_list.php";

    String INTENT_EXTRA_PRODUCT_DETAILS = "INTENT_EXTRA_PRODUCT_DETAILS";

    String MESSAGE_PRODUCT_ADDED_TO_CART = "Product Added To Cart Successfully!";
    String ERROR_MESSAGE_PRODUCT_ADDED_TO_CART = "Product is already in the cart.";

    String MESSAGE_PRODUCT_REMOVED_FROM_CART = "Product Removed From Cart";
    String ERROR_MESSAGE_PRODUCT_REMOVED_FROM_CART = "Unable To Remove Product From Cart";
    String MESSAGE_NO_ITEMS_FOR_THIS_CATEGORY = "Sorry. No Items Found.";

    String ERROR_MESSAGE_NO_INTERNET_CONNECTION = "No Internet Connection Found!!";
    String ERROR_MESSAGE_SERVER_ERROR = "We are facing some technical glitch at our server. Please try again later.";
    String ERROR_MESSAGE_EMPTY_CART = "Empty Cart.";
}
