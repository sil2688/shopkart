package com.shopkart.app.classes;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by salil on 2/3/16.
 */
public class Product implements Parcelable {

    private String name;
    private Amount price;
    private String imageUrl;
    private String category;
    private int quantity;

    public Product(String name, Amount price) {
        this.name = name;
        this.price = price;

        if (price == null) {
            this.price = new Amount(0);
        }
    }

    public Product(String name, Amount price, String imageUrl) {
        this(name, price);
        this.imageUrl = imageUrl;
    }

    public Product(String name, Amount price, String imageUrl, String category, int quantity) {
        this(name, price, imageUrl);
        this.category = category;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public Amount getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getCategory() {
        return category;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return imageUrl.equals(product.imageUrl);
    }

    @Override
    public int hashCode() {
        return imageUrl.hashCode();
    }

    /*
    Sample Response
    {
        name: "LG 28 Ltr BX1345",
        price: "15999.00",
        imageUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/NN-K125MBGPG_Grill-Mikrowelle_silber_Panasonic.png/220px-NN-K125MBGPG_Grill-Mikrowelle_silber_Panasonic.png",
        category: "Oven"
    }
     */

    /**
     * Get the Product Object from the JSON Object.
     *
     * @param jsonObject
     * @return
     */
    public static Product fromJSONObject(JSONObject jsonObject) {
        Product product = null;

        if (jsonObject != null) {
            String name = jsonObject.optString("name");
            String price = jsonObject.optString("price");
            String imageUrl = jsonObject.optString("imageUrl");
            String category = jsonObject.optString("category");
            int quantity = jsonObject.optInt("quantity", 0);

            Amount amount = new Amount(Double.valueOf(price));

            product = new Product(name, amount, imageUrl, category, quantity);
        }

        return product;
    }

    public static List<Product> fromJSONArray(JSONArray jsonArray) {
        List<Product> productList = null;
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                Product product = fromJSONObject(jsonArray.optJSONObject(i));
                if (product != null) {

                    if (productList == null) {
                        productList = new ArrayList<>();
                    }

                    productList.add(product);
                }
            }
        }

        return productList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeParcelable(this.price, 0);
        dest.writeString(this.imageUrl);
        dest.writeString(this.category);
        dest.writeInt(this.quantity);
    }

    protected Product(Parcel in) {
        this.name = in.readString();
        this.price = in.readParcelable(Amount.class.getClassLoader());
        this.imageUrl = in.readString();
        this.category = in.readString();
        this.quantity = in.readInt();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
