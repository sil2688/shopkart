package com.shopkart.app.classes;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.shopkart.app.Callback;
import com.shopkart.app.SKError;

/**
 * Created by salil on 5/3/16.
 */
public final class Utils {

    public static void showSnackbar(View parentView, String message) {
        Snackbar.make(parentView, message, Snackbar.LENGTH_SHORT).show();
    }

    public static <T> void sendResponse(Callback<T> callback, T t) {
        if (callback != null) {
            callback.success(t);
        }
    }

    public static void sendError(Callback callback, SKError error) {
        if (callback != null) {
            callback.error(error);
        }
    }
}
