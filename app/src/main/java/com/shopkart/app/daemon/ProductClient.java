package com.shopkart.app.daemon;

import android.content.Context;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.shopkart.app.Callback;
import com.shopkart.app.SKError;
import com.shopkart.app.classes.Constants;
import com.shopkart.app.classes.Product;
import com.shopkart.app.classes.Utils;

import org.json.JSONArray;

import java.util.List;

/**
 * Created by salil on 5/3/16.
 */
public class ProductClient {
    private static ProductClient mClient = null;
    private NetworkManager mNetworkManager = null;
    private Context mContext;

    private ProductClient(Context context) {
        mContext = context;
        mNetworkManager = NetworkManager.getInstance(context);
    }

    public static ProductClient getClient(Context context) {
        if (mClient == null) {
            synchronized (ProductClient.class) {
                if (mClient == null) {
                    mClient = new ProductClient(context);
                }
            }
        }

        return mClient;
    }


    public void destroy() {
        mClient = null;
        mNetworkManager.destroy();
    }

    /**
     * Get map of all the products against the category.
     *
     * @param category
     * @param callback
     */
    public void getAllProducts(String category, final Callback<List<Product>> callback) {
        String url = Constants.URL_PRODUCT_LIST + "?category=" + category;
        JsonArrayRequest request = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray array) {
                        List<Product> productList = Product.fromJSONArray(array);
                        if (productList != null) {
                            Utils.sendResponse(callback, productList);
                        } else {
                            Utils.sendError(callback, new SKError(Constants.MESSAGE_NO_ITEMS_FOR_THIS_CATEGORY));
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.sendError(callback, new SKError(error));
            }
        });

        mNetworkManager.addToRequestQueue(request);
    }
}
