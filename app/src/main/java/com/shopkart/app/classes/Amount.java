package com.shopkart.app.classes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by salil on 2/3/16.
 */
public class Amount implements Parcelable {
    private double value = 0;
    private String currency;

    public Amount(double value) {
        this.value = value;
        this.currency = "INR";
    }

    public Amount(double value, String currency) {
        this.value = value;
        this.currency = currency;
    }

    public double getValue() {
        return value;
    }

    public void addAmount(Amount amount) {
        if (amount != null) {
            this.value += amount.getValue();
        }
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.value);
        dest.writeString(this.currency);
    }

    protected Amount(Parcel in) {
        this.value = in.readDouble();
        this.currency = in.readString();
    }

    public static final Parcelable.Creator<Amount> CREATOR = new Parcelable.Creator<Amount>() {
        public Amount createFromParcel(Parcel source) {
            return new Amount(source);
        }

        public Amount[] newArray(int size) {
            return new Amount[size];
        }
    };
}
