package com.shopkart.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopkart.app.adapter.ShoppingCartRecyclerViewAdapter;
import com.shopkart.app.classes.Amount;
import com.shopkart.app.classes.Constants;
import com.shopkart.app.classes.Product;
import com.shopkart.app.classes.ShoppingCart;
import com.shopkart.app.classes.Utils;
import com.shopkart.app.ui.SKRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ShoppingCartActivity extends AppCompatActivity implements ShoppingCartRecyclerViewAdapter.OnRecyclerviewInteractionListener {

    @Bind(R.id.layout_parent)
    View mParentView;
    @Bind(R.id.rv_products)
    SKRecyclerView mRVProducts;
    @Bind(R.id.txt_total_cart_amount)
    TextView mTxtTotalCartAmount;

    private ShoppingCartRecyclerViewAdapter mAdapter = null;
    private List<Product> mProductList = null;
    private ShoppingCart mShoppingCart = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);

        ButterKnife.bind(this);

        mProductList = new ArrayList<>();
        mAdapter = new ShoppingCartRecyclerViewAdapter(this, mProductList, this);
        mRVProducts.setLayoutManager(new LinearLayoutManager(this));
        mRVProducts.setAdapter(mAdapter);
        mRVProducts.setNestedScrollingEnabled(false);
        mRVProducts.setHasFixedSize(true);
        mRVProducts.setItemAnimator(null);

        mShoppingCart = ShoppingCart.getCart(this);

        fetchProductList();

        setTitle("Your Cart");
    }

    private void fetchProductList() {
        mShoppingCart.getProductList(new Callback<List<Product>>() {
            @Override
            public void success(List<Product> productList) {
                mProductList.addAll(productList);
                // Notify the adapter that new data has been added.
                mAdapter.notifyDataSetChanged();
                // Tell recyclerview that load more is completed.
                mRVProducts.loadMoreComplete();

                // Set the total Cart Amount
                Amount totalAmount = mShoppingCart.getTotalAmount();
                String totalCartAmount = String.format("%.2f", totalAmount.getValue());
                mTxtTotalCartAmount.setText("Total : " + getString(R.string.Rs) + " " + totalCartAmount);
                mTxtTotalCartAmount.setVisibility(View.VISIBLE);
            }

            @Override
            public void error(SKError error) {
                // TODO: Show Proper message in case of empty list.

                mTxtTotalCartAmount.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onItemClick(ImageView imageView, TextView txtImageTitle, Product product) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_PRODUCT_DETAILS, product);
        startActivity(intent);
    }

    @Override
    public void removeProductFromCart(Product product) {
        ShoppingCart shoppingCart = ShoppingCart.getCart(this);
        shoppingCart.removeProductFromCart(product, new Callback<SKResponse>() {
            @Override
            public void success(SKResponse skResponse) {
                Utils.showSnackbar(mParentView, skResponse.getMessage());

                fetchProductList();
            }

            @Override
            public void error(SKError error) {
                Utils.showSnackbar(mParentView, error.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
