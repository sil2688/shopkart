package com.shopkart.app;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopkart.app.classes.Constants;
import com.shopkart.app.classes.Product;
import com.shopkart.app.classes.ShoppingCart;
import com.shopkart.app.classes.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductDetailsActivity extends AppCompatActivity {

    @Bind(R.id.iv_product_photo)
    ImageView mIVProductPhoto;

    @Bind(R.id.txt_product_name)
    TextView mTxtProductName;

    @Bind(R.id.txt_product_price)
    TextView mTxtProductPrice;

    @Bind(R.id.layout_parent)
    FrameLayout mLayoutParent;

    private Product mProduct;
    private ShoppingCart mCart;
    private Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        ButterKnife.bind(this);

        mProduct = getIntent().getParcelableExtra(Constants.INTENT_EXTRA_PRODUCT_DETAILS);
        mCart = ShoppingCart.getCart(mContext);

        if (mProduct != null) {
            Glide.with(mContext)
                    .load(mProduct.getImageUrl())
                    .into(mIVProductPhoto);

            mTxtProductName.setText(mProduct.getName());
            mTxtProductPrice.setText(String.format("%s %.2f", mContext.getString(R.string.Rs), mProduct.getPrice().getValue()));
        }

        setTitle(mProduct.getCategory());
    }

    @OnClick(R.id.fab_add_to_cart)
    public void addProductToCart() {
        mCart.addProductToCart(mProduct, new Callback<SKResponse>() {
            @Override
            public void success(SKResponse skResponse) {
                Utils.showSnackbar(mLayoutParent, skResponse.getMessage());
            }

            @Override
            public void error(SKError error) {
                Utils.showSnackbar(mLayoutParent, error.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
