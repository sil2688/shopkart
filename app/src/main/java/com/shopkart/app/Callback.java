package com.shopkart.app;

/**
 * Created by salil on 5/3/16.
 */
public interface Callback<T> {
    void success(T t);

    void error(SKError error);
}
