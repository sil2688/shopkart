package com.shopkart.app.ui;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.shopkart.app.Callback;
import com.shopkart.app.ProductDetailsActivity;
import com.shopkart.app.R;
import com.shopkart.app.SKError;
import com.shopkart.app.adapter.SKRecyclerViewAdapter;
import com.shopkart.app.classes.Constants;
import com.shopkart.app.classes.Product;
import com.shopkart.app.daemon.ProductClient;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductListFragment extends Fragment implements SKRecyclerViewAdapter.OnRecyclerviewInteractionListener {
    private ProductClient mProductClient = null;

    @Bind(R.id.rv_products)
    SKRecyclerView mRVProducts;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;
    @Bind(R.id.txt_no_items)
    TextView mTxtNoItems;

    private SKRecyclerViewAdapter mAdapter = null;
    private List<Product> mProductList = null;
    private String mCategory = null;
    private static final String INTENT_EXTRA_CATEGORY = "INTENT_EXTRA_CATEGORY";
    private CountDownTimer mTimer = null;

    public ProductListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param category
     * @return A new instance of fragment ProductListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductListFragment newInstance(String category) {
        ProductListFragment fragment = new ProductListFragment();
        Bundle args = new Bundle();
        args.putString(INTENT_EXTRA_CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCategory = getArguments().getString(INTENT_EXTRA_CATEGORY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        ButterKnife.bind(this, view);

        mProductList = new ArrayList<>();
        mProductClient = ProductClient.getClient(getActivity());
        mAdapter = new SKRecyclerViewAdapter(getActivity(), mProductList, this);

        mRVProducts.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mRVProducts.setAdapter(mAdapter);
        mRVProducts.setNestedScrollingEnabled(false);
        mRVProducts.setHasFixedSize(true);
        mRVProducts.setItemAnimator(null);

        fetchProductList();
        showProgressBar();

        return view;
    }

    private void fetchProductList() {
        mProductClient.getAllProducts(mCategory, new Callback<List<Product>>() {
            @Override
            public void success(List<Product> productList) {
                mProductList.addAll(productList);
                // Notify the adapter that new data has been added.
                mAdapter.notifyDataSetChanged();
                // Tell recyclerview that load more is completed.
                mRVProducts.loadMoreComplete();

                hideProgressBar();

                mTxtNoItems.setVisibility(View.GONE);
            }

            @Override
            public void error(SKError error) {
                hideProgressBar();

                mTxtNoItems.setText(error.getMessage());
                mTxtNoItems.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        hideProgressBar();
        progressBar = null;
    }

    @Override
    public void onItemClick(ImageView imageView, TextView txtImageTitle, Product product) {
        Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_PRODUCT_DETAILS, product);
        startActivity(intent);
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        startTimer();
    }

    private void startTimer() {

        mTimer = new CountDownTimer(10000, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
                // Change the color of the progressBar
                // #FF0000 #F7FE2E #088A08 #0A2229
                String colorCode = "#FF0080";
                switch ((int) (millisUntilFinished / 1000)) {
                    case 1:
                    case 6:
                        colorCode = "#0040FF";
                        break;
                    case 2:
                    case 7:
                        colorCode = "#FF0000";
                        break;
                    case 3:
                    case 8:
                        colorCode = "#AEB404";
                        break;
                    case 4:
                    case 9:
                        colorCode = "#088A08";
                        break;
                    case 5:
                    case 0:
                        colorCode = "#4000FF";
                        break;
                }
                progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor(colorCode), PorterDuff.Mode.MULTIPLY);
            }

            @Override
            public void onFinish() {
                // Change the color of the progressBar
                progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#A9BCF5"), PorterDuff.Mode.MULTIPLY);

                startTimer();
            }
        }.start();
    }

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        stopTimer();
    }


}
