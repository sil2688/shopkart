package com.shopkart.app.adapter;

import android.content.Context;
import android.support.annotation.AnimRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopkart.app.R;
import com.shopkart.app.classes.Product;

import java.util.List;


public class SKRecyclerViewAdapter extends RecyclerView.Adapter<SKRecyclerViewAdapter.VHItem> {

    private final Context mContext;
    private final List<Product> mProductList;
    private final OnRecyclerviewInteractionListener mListener;
    private int lastPosition = -1;

    public SKRecyclerViewAdapter(Context context, List<Product> productList, OnRecyclerviewInteractionListener listener) {
        mContext = context;
        mProductList = productList;
        mListener = listener;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return (int) System.currentTimeMillis();
    }

    @Override
    public SKRecyclerViewAdapter.VHItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_product_list, parent, false);
        return new VHItem(view);
    }

    @Override
    public void onBindViewHolder(final SKRecyclerViewAdapter.VHItem holder, int position) {
        Product product = getItem(position);

        if (product != null) {
            final String imageUrl = product.getImageUrl();
            holder.mProduct = product;
            holder.mTitleView.setText(product.getName());

            Glide.with(mContext)
                    .load(imageUrl)
                    .into(holder.mImageView);

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mListener) {
                        // Notify the active callbacks interface (the activity, if the
                        // fragment is attached to one) that an item has been selected.
                        mListener.onItemClick(holder.mImageView, holder.mTitleView, holder.mProduct);
                    }
                }
            });

            // Here you apply the animation when the view is bound
            setAnimation(holder.mView, position, R.anim.slide_up);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position, @AnimRes int animationId) {
        if (position > lastPosition) {
            // If the bound view wasn't previously displayed on screen, it's animated
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), animationId);
            viewToAnimate.startAnimation(animation);

            lastPosition = position;
        }
    }

    private Product getItem(int position) {
        if (mProductList != null && position < mProductList.size()) {
            return mProductList.get(position);
        }

        return null;
    }

    @Override
    public int getItemCount() {
        return mProductList != null ? mProductList.size() : 0;
    }

    public class VHItem extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitleView;
        public final ImageView mImageView;
        public Product mProduct;

        public VHItem(View view) {
            super(view);
            mView = view;
            mTitleView = (TextView) view.findViewById(R.id.txt_product_name);
            mImageView = (ImageView) view.findViewById(R.id.iv_product_photos);
        }
    }

    public interface OnRecyclerviewInteractionListener {
        void onItemClick(ImageView imageView, TextView txtImageTitle, Product product);
    }
}