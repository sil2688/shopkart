package com.shopkart.app;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shopkart.app.adapter.SKRecyclerViewAdapter;
import com.shopkart.app.classes.Constants;
import com.shopkart.app.classes.Product;
import com.shopkart.app.ui.ProductListFragment;
import com.shopkart.app.ui.SKRecyclerView;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SKRecyclerViewAdapter.OnRecyclerviewInteractionListener, SKRecyclerView.NTRecyclerViewListener {

    private FragmentManager mFragmentManager = null;

    private static final String ACTIVITY_TITLE = "Electronics & Furniture";
    public static final String CATEGORY_ALL = "ALL";
    public static final String CATEGORY_FURNITURE = "Furniture";
    public static final String CATEGORY_ELECTRONICS = "Electronics";
    private String mCurrentCategory = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_shopping_cart);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ShoppingCartActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        mFragmentManager = getFragmentManager();

        showProductListFragment(CATEGORY_ALL);
    }

    private void showProductListFragment(String category) {
        mCurrentCategory = category;

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction()

                .replace(R.id.container, ProductListFragment.newInstance(category));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        String title = getString(R.string.app_name);
        if (CATEGORY_ALL.equals(category)) {
            title = ACTIVITY_TITLE;
        } else if (CATEGORY_ELECTRONICS.equals(category)) {
            title = CATEGORY_ELECTRONICS;
        } else if (CATEGORY_FURNITURE.equals(category)) {
            title = CATEGORY_FURNITURE;
        }

        setTitle(title);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_category_all) {
            showProductListFragment(CATEGORY_ALL);
        } else if (id == R.id.action_category_furniture) {
            showProductListFragment(CATEGORY_FURNITURE);
        } else if (id == R.id.action_category_electronics) {
            showProductListFragment(CATEGORY_ELECTRONICS);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemClick(ImageView imageView, TextView txtImageTitle, Product product) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_PRODUCT_DETAILS, product);
        startActivity(intent);
    }

    @Override
    public void onLoadMore() {

    }
}
