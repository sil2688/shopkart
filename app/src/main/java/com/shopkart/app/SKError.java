package com.shopkart.app;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.shopkart.app.classes.Constants;

/**
 * Created by salil on 5/3/16.
 */
public class SKError extends SKResponse {

    public SKError(VolleyError error) {
        super();
        if (error instanceof TimeoutError) {
            this.mMessage = Constants.MESSAGE_NO_ITEMS_FOR_THIS_CATEGORY;
        } else if (error instanceof NoConnectionError) {
            this.mMessage = Constants.ERROR_MESSAGE_NO_INTERNET_CONNECTION;
        } else if (error instanceof ServerError) {
            this.mMessage = Constants.ERROR_MESSAGE_SERVER_ERROR;
        } else {
            this.mMessage = error.getMessage();
        }
    }

    public SKError(String message) {
        super(message);
    }
}
