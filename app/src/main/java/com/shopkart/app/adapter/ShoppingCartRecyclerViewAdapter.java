package com.shopkart.app.adapter;

import android.content.Context;
import android.support.annotation.AnimRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shopkart.app.R;
import com.shopkart.app.classes.Amount;
import com.shopkart.app.classes.Product;

import java.util.List;


public class ShoppingCartRecyclerViewAdapter extends RecyclerView.Adapter<ShoppingCartRecyclerViewAdapter.VHItem> {

    private final Context mContext;
    private final List<Product> mProductList;
    private final OnRecyclerviewInteractionListener mListener;
    private int lastPosition = -1;

    public ShoppingCartRecyclerViewAdapter(Context context, List<Product> productList, OnRecyclerviewInteractionListener listener) {
        mContext = context;
        mProductList = productList;
        mListener = listener;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return (int) System.currentTimeMillis();
    }

    @Override
    public ShoppingCartRecyclerViewAdapter.VHItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_shopping_cart_list, parent, false);
        return new VHItem(view);
    }

    @Override
    public void onBindViewHolder(final VHItem holder, int position) {
        final Product product = getItem(position);

        final String imageUrl = product.getImageUrl();
        holder.mProduct = product;
        holder.txtProductName.setText(product.getName());
        holder.txtProductQuantity.setText(String.format("Quantity : (%d)", product.getQuantity()));
        Amount price = product.getPrice();
        if (price != null) {
            double totalPrice = price.getValue() * product.getQuantity();
            holder.txtProductPrice.setText(String.format("%s %.2f", mContext.getString(R.string.Rs), totalPrice));
        }

        Glide.with(mContext)
                .load(imageUrl)
                .into(holder.imgProductPhoto);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onItemClick(holder.imgProductPhoto, holder.txtProductName, holder.mProduct);
                }
            }
        });

        holder.btnRemoveFromCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.removeProductFromCart(product);
                }
            }
        });

        // Here you apply the animation when the view is bound
        setAnimation(holder.view, position, R.anim.slide_up);
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position, @AnimRes int animationId) {
        if (position > lastPosition) {
            // If the bound view wasn't previously displayed on screen, it's animated
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), animationId);
            viewToAnimate.startAnimation(animation);

            lastPosition = position;
        }
    }

    private Product getItem(int position) {
        if (mProductList != null && position < mProductList.size()) {
            return mProductList.get(position);
        }

        return null;
    }

    @Override
    public int getItemCount() {
        return mProductList != null ? mProductList.size() : 0;
    }

    public class VHItem extends RecyclerView.ViewHolder {
        public final View view;
        public final ImageView imgProductPhoto;
        public final TextView txtProductName;
        public final TextView txtProductQuantity;
        public final TextView txtProductPrice;
        public final Button btnRemoveFromCart;
        public Product mProduct;

        public VHItem(View view) {
            super(view);
            this.view = view;
            imgProductPhoto = (ImageView) view.findViewById(R.id.iv_product_photos);
            txtProductName = (TextView) view.findViewById(R.id.txt_product_name);
            txtProductQuantity = (TextView) view.findViewById(R.id.txt_product_quantity);
            txtProductPrice = (TextView) view.findViewById(R.id.txt_product_price);
            btnRemoveFromCart = (Button) view.findViewById(R.id.btn_remove_from_cart);
        }
    }

    public interface OnRecyclerviewInteractionListener {
        void onItemClick(ImageView imageView, TextView txtImageTitle, Product product);

        void removeProductFromCart(Product product);
    }
}