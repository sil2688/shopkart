package com.shopkart.app.database;

import android.provider.BaseColumns;

/**
 * Created by salil on 4/3/16.
 */
public final class CartDataContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public CartDataContract() {
    }

    /* Inner class that defines the table contents */
    public static abstract class CartEntry implements BaseColumns {
        public static final String TABLE_NAME = "cart";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_PRICE = "price";
        public static final String COLUMN_NAME_IMAGE_URL = "image_url";
        public static final String COLUMN_NAME_CATEGORY = "category";
        public static final String COLUMN_NAME_QUANTITY = "quantity";
    }
}