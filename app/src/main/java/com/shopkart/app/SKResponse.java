package com.shopkart.app;

/**
 * Created by salil on 5/3/16.
 */
public class SKResponse {

    protected String mMessage;

    public SKResponse(String message) {
        mMessage = message;
    }

    protected SKResponse() {

    }

    public String getMessage() {
        return mMessage;
    }
}
